# frozen_string_literal: true

module Security
  class ProjectStatistics < Gitlab::Database::SecApplicationRecord
    self.primary_key = :project_id
    self.table_name = 'project_security_statistics'

    belongs_to :project, optional: false

    scope :by_projects, ->(project_ids) { where(project_id: project_ids) }

    class << self
      def create_for(project)
        upsert({ project_id: project.id })

        find_by_project_id(project.id)
      end
    end

    def increase_vulnerability_counter!(increment)
      self.class.by_projects(project_id).update_all("vulnerability_count = vulnerability_count + #{increment}")
    end

    def decrease_vulnerability_counter!(decrement)
      self.class.by_projects(project_id).update_all("vulnerability_count = vulnerability_count - #{decrement}")
    end
  end
end
