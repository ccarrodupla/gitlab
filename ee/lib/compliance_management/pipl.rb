# frozen_string_literal: true

module ComplianceManagement
  module Pipl
    COVERED_COUNTRY_CODES = %w[CN HK MO].freeze
    PIPL_SUBJECT_USER_CACHE_KEY = 'pipl_subject_user'
  end
end
